package plot3d

import (
	"fmt"
	"io"
	"math"
)

var svg string

const (
	width, height = 1280, 800
	cells         = 100
	xyrange       = 20
	xyscale       = width / 2 / xyrange
	zscale        = height * 0.4
	angle         = math.Pi / 6
)

type color struct {
	r, g, b uint8
}

var sin30, cos30 = math.Sin(angle), math.Cos(angle)
var palette [256]color

func init() {
	for i := range palette {
		c := &palette[i]

		switch {
		case i < 128:
			c.r = uint8(i)
			c.g = uint8(i / 5)
			c.b = uint8(2 * i)
		case i >= 128:
			red := float32(i) * 1.02
			if red > 255 {
				red = 255
				c.g = uint8(i / 4)
			}
			c.r = uint8(red)

			blue := 255 - (i-128)*2
			if blue < 0 {
				blue = 0
			}
			c.b = uint8(blue)
		}
	}
}

func svgBegin(width, height int) {
	svg = fmt.Sprintf(`<?xml version="1.0" encoding="UTF-8" standalone="no"?>
	<svg version = "1.1"
		 baseProfile="full"
		 xmlns = "http://www.w3.org/2000/svg" 
		 xmlns:xlink = "http://www.w3.org/1999/xlink"
		 xmlns:ev = "http://www.w3.org/2001/xml-events"
		 style="stroke: gray; fill: white; stroke-width: 0.7;"
		 height = "%dpx"  width = "%dpx">
	`, height, width)

	cellheight := float64(height / 255)
	cellwidth := float64(24)
	for i, c := range palette {
		y := float64(float64(height) - cellheight*float64(i))
		svgAddPolygon(0, y, cellwidth, y, cellwidth, y+cellheight, 0, y+cellheight, c.r, c.g, c.b)
	}
}

func svgEnd() {
	svg += "</svg>"
}

func svgAddPolygon(ax, ay, bx, by, cx, cy, dx, dy float64, r, g, b uint8) {
	webcolor := fmt.Sprintf("#%02X%02X%02X", r, g, b)
	svg += fmt.Sprintf(`<polygon points="%g %g %g %g %g %g %g %g" style="fill: %s; fill-opacity: 0.8;"></polygon>`,
		ax, ay, bx, by, cx, cy, dx, dy, webcolor)
}

func Draw3DPlotSVG(w io.Writer) {
	svgBegin(width, height)
	for i := 0; i < cells; i++ {
		for j := 0; j < cells; j++ {
			ax, ay, _ := getCellCorner(i+1, j)
			bx, by, z := getCellCorner(i, j)
			cx, cy, _ := getCellCorner(i, j+1)
			dx, dy, _ := getCellCorner(i+1, j+1)
			if z < 0 {
				z = 0
			}
			c := palette[uint8(z*255)]
			svgAddPolygon(ax, ay, bx, by, cx, cy, dx, dy, c.r, c.g, c.b)
		}
	}
	svgEnd()
	fmt.Fprint(w, svg)
}

func getCellCorner(i, j int) (float64, float64, float64) {
	x := xyrange * (float64(i)/cells - 0.5)
	y := xyrange * (float64(j)/cells - 0.5)
	z := f(x, y)
	sx := width/2 + (x-y)*cos30*xyscale
	sy := height/2 + (x+y)*sin30*xyscale - z*zscale
	return sx, sy, z
}

func f(x, y float64) float64 {
	r := math.Hypot(x, y)
	return math.Sin(r) / r
	return r
}
