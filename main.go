package main

import (
	"log"
	"net/http"
	"plot3d/plot3d"
)

func main() {
	http.HandleFunc("/plot3d", func(w http.ResponseWriter, r *http.Request) {
		log.Print("Generating plot...")
		plot3d.Draw3DPlotSVG(w)
		log.Printf("Done")
	})
	log.Fatal(http.ListenAndServe(":8080", nil))
}
